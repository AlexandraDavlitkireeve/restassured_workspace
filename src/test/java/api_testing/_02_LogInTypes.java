package api_testing;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class _02_LogInTypes {

    /**
     * Log in to Octoperf and use full path to perform a sihn in
     *
     */

    @Test
    public void fullPath() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    /**
     * Log in using map to verify content type and status code
     * Map stores -> key and value pairs
     * Hashmap implements Maps
     */
    @Test
    public void useMapsToLogIn() {

        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        String username = "alexandra.davlitkireeva@gmail.com";
        String password = "@Region174@";

        //Writing Map with String, String values
        Map<String, Object> map= new HashMap<>();
        map.put("username", username);
        map.put("password", password);

        RestAssured.given()
                .queryParams(map)
                .when()
                .post(path)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
    }
    @Test
    public void useParam() {
        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        String username = "alexandra.davlitkireeva@gmail.com";
        String password = "@Region174@";

        RestAssured.given()
                .queryParam("username", username)
                .queryParam("password", password)
                .when()
                .post(path)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
    }
}
