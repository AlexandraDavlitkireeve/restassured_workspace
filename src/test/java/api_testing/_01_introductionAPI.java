package api_testing;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

public class _01_introductionAPI {

    // Rest Assured is a Java library that used to perform API tests

    // It uses BDD style such as given, when, then, and ...
    // Rest Assured has methods to fetch data
    // from the response body when we make a request.
    // As part of Rest Assured we also perform CRUD operations:
    // CREATE, READ, UPDATE, DELETE -> POST, GET, PUT, DELETE
    // As test Engineers we mostly use GET and POST. If needed we use PUT and DELETE


    // NOTE: as QA Testers we mostly work with JSON body payloads, we also have exposure to XML
    // JSON EXAMPLE:
    // {"name":"TLA"}

    // Some of the most used methods in rest Assured are:
    // When we make a request:
    // given() ---> use to prepare a request
    // when() ---> use to send a request
    // then() ---> use to verify a request

    // When we verify a request:
    // prettyPrint() --> use to print a response in pretty format
    // prettyPeek() --> use to print a response headers, url, in the console in a pretty format
    // log() --> logs use to print a response
    // asString() --> Allows us to print a string format
    // contentType() --> use to verify a content type from a response body and when we send a request
    // accept() --> use to verify a response from the head when making a GET request

    // baseURI --> use to verify body response from the header when making a request
    public static String baseURI = "https://api.octoperf.com/";

    // Path --> when we make a request we only provide the path(endpoint) to the specific baseURI
    private String path = "public/users/login";

    //      Full URI = https://api.octoperf.com/public/users/login

    //      What is an endpoint?
    //      An endpoint is a unique URL that represent the object or collect of objects.

    //      Query Params?
    //            Example: https://www.amazon.com/s?k=christmas+tree
    //                           BaseURI       Endpoint   ? Query params

    // Task: Make a http request: POST request for login to octoperf
    // UNIT Tests
    @Test
    public void printResponse() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .prettyPrint();
    }

    @Test
    public void printResponse1() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .prettyPeek();
    }

    /**
     *  When we verify a response body we also verify the Status codes:
     *  1xx --> Information
     *  2xx --> Success (200 -> OK, 201 -> Created, 204 -> No content)
     *  3xx --> Redirection
     *  4xx --> Client error (400, 401, 403, 404)
     *  5xx --> Server error (500 -> Internal Server error)
     */

    // Task: validate response status code for log in

    @Test
    public void verifyStatusCode() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .then()
                .assertThat()
                .statusCode(200);

        System.out.println("Test Status code for log in passed");

    }
    // Task: Validate Content-type JSON from the response body
    @Test
    public void verifyContentTypeJSON() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .prettyPeek()
                .then()
                .assertThat()
                .contentType(ContentType.JSON);
        System.out.println("Content-type JSON is verified successfully");
    }

    @Test
    public void verifyContentTypeJSONandStatusCode() {
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?username=alexandra.davlitkireeva@gmail.com&password=@Region174@")
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
        System.out.println("Content-type JSON and Status Code are verified successfully");
    }
}
