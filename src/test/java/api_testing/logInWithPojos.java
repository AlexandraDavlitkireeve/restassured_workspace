package api_testing;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import pojos.loginPojos;

public class logInWithPojos {

    loginPojos pojo = new loginPojos();

    @Test
    public void loginPojos() {
        pojo.setUserName("alexandra.davlitkireeva@gmail.com");
        pojo.setPassWord("@Region174@");

        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        RestAssured.given()
                .queryParam("username", pojo.getUserName())
                .queryParam("password", pojo.getPassWord())
                .when()
                .post(path)
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON)
                .and()
                .log().all();
    }
}
