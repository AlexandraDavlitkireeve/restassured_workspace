package practice;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

public class day_2 {

    @Test
    public void findPetsByStatusContentAndCode(){
        RestAssured.given()
                .when()
                .get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
    }
    @Test
    public void findStoreInventoryContentAndCode(){
        RestAssured.given()
                .when()
                .get("https://petstore.swagger.io/v2/store/inventory")
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
    }
    @Test
    public void getPosts() {
        RestAssured.given()
                .when()
                .get("https://jsonplaceholder.typicode.com/posts")
                .prettyPrint();
    }
    @Test
    public void postsContentAndCode(){
        RestAssured.given()
                .when()
                .get("https://jsonplaceholder.typicode.com/posts")
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);
    }
    @Test
    public void getPostsID() {
        RestAssured.given()
                .when()
                .get("https://jsonplaceholder.typicode.com/posts/15")
                .prettyPrint();
    }
    @Test
    public void postsIDContentAndCode(){
        RestAssured.given()
                .when()
                .get("https://jsonplaceholder.typicode.com/posts/15")
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .and()
                .statusCode(200);

    }
}
