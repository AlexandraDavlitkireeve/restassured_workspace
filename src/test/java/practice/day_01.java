package practice;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

public class day_01 {
    @Test
    public void printAuthorsResponse() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .prettyPrint();
    }
    @Test
    public void verifyStatusCodeAuthor() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .then()
                .assertThat()
                .statusCode(200);

        System.out.println("Test Status code for log in passed");
    }
    @Test
    public void verifyContentTypeJSONAuthor() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .then()
                .assertThat()
                .contentType(ContentType.JSON);
        System.out.println("Content-type JSON is verified successfully");
    }
    @Test
    public void printPeekAuthorsResponse() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .prettyPeek();
    }

    @Test
    public void printActivityResponse() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .prettyPrint();
    }

    @Test
    public void verifyStatusCodeActivity() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then()
                .assertThat()
                .statusCode(200);

        System.out.println("Test Status code for log in passed");
    }
    @Test
    public void verifyContentTypeJSONActivity() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then()
                .assertThat()
                .contentType(ContentType.JSON);
        System.out.println("Content-type JSON is verified successfully");
    }
    @Test
    public void printPeekActivityResponse() {
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .prettyPeek();
    }
}
