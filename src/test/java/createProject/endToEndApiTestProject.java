package createProject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.apache.http.HttpStatus.*;

public class endToEndApiTestProject {

    Response response;
    Map<String, Object> variables;
    String memberOf = "/workspaces/member-of";
    //String projectList = "/workspaceId/DESIGN";
    String workspaceId;
    String created;
    String description;
    String id;
    String lastModified;
    String name;
    String userId;

    String projectId;

    // In TestNG what is one of the annotations that allows us to runs the Tests before each test

    @BeforeTest
    public String token() {
        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        Map<String, Object> map = new HashMap<>();
        map.put("username", "alexandra.davlitkireeva@gmail.com");
        map.put("password", "@Region174@");

        return RestAssured.given()
                .queryParams(map)
                .when()
                .post(path)
                .then()
                .statusCode(SC_OK)
                .extract()// Method that extracts the response JSON DATA
                .body()// Body extracted as a JSON format
                .jsonPath()// Navigate using jsonPath
                .get("token"); // Get a value as a key token
    }
    @Test
    public void checkToken() {
        System.out.println(token());
    }

    @Test
    public void createWorkspace() {

        String createWorkspace = "{\"id\":\"\",\"created\":\"2022-11-16T00:21:51.664Z\",\"lastModified\":\"2022-11-16T00:21:51.664Z\",\"userId\":\"\",\"name\":\"Personal Bank Account\",\"description\":\"Saving money\"}";

        System.out.println(createWorkspace);

        response = RestAssured.given()
                .headers("Content-type", "application/json")
                .header("Authorization", token())
                .and()
                .body(createWorkspace)
                .when()
                .post("/workspaces")
                .then()
                .log().all()
                .extract()
                .response();

        Assert.assertEquals(SC_CREATED, response.statusCode());
    }
    @Test
    public void memberOf() {
        response = RestAssured.given()
                .header("Authorization", token())
                .when()
                .get(memberOf)
                .then()
                .statusCode(200)
                .log().all()
                .extract()
                .response();
        System.out.println(response.jsonPath().getString("name[0]"));
        System.out.println(response.jsonPath().getString("name[1]"));

        Assert.assertEquals(SC_OK, response.statusCode());

        Assert.assertEquals("Personal Bank Account", response.jsonPath().getString("name[1]"));
        Assert.assertEquals("Saving money", response.jsonPath().getString("description[1]"));
        //Assert.assertEquals("1668558111664", response.jsonPath().getString("created[1]"));

        created = response.jsonPath().getString("created[1]");
        description = response.jsonPath().getString("description[1]");
        id = response.jsonPath().getString("id[1]");
        lastModified = response.jsonPath().getString("lastModified[1]");
        name = response.jsonPath().getString("name[1]");
        userId = response.jsonPath().getString("userId[1]");

        //Assert.assertEquals(created, response.jsonPath().getString("created[1]"));
        Assert.assertEquals(description, response.jsonPath().getString("description[1]"));
        Assert.assertEquals(id, response.jsonPath().getString("id[1]"));
        Assert.assertEquals(lastModified, response.jsonPath().getString("lastModified[1]"));
        Assert.assertEquals(name, response.jsonPath().getString("name[1]"));
        Assert.assertEquals(userId, response.jsonPath().getString("userId[1]"));

        System.out.println(id);
        System.out.println(userId);

        variables = new HashMap<>();
        variables.put("created", created);
        variables.put("description", description);
        variables.put("id", id);
        variables.put("lastModified", lastModified);
        variables.put("name", name);
        variables.put("userId", userId);

    }
    @Test(dependsOnMethods = {"memberOf"})
    public void createProject() {
        String requestBody = "{\"id\":\"\",\"created\":\"2022-11-09T01:58:24.835Z\",\"lastModified\":\"2022-11-09T01:58:24.835Z\",\"userId\":\""+variables.get("userId")+"\",\"workspaceId\":\""+variables.get("id")+"\",\"name\":\"Savings Account\",\"description\":\"Save money account\",\"type\":\"DESIGN\",\"tags\":[]}";
        System.out.println(requestBody);
        System.out.println("---> " + variables.get("id"));
        System.out.println("---> " + variables.get("userId"));


        response = RestAssured.given()
                .headers("Content-type", "application/json")
                .header("Authorization", token())
                .and()
                .body(requestBody)
                .when()
                .post("/design/projects")
                .then()
                .log().all()
                .extract()
                .response();

        Assert.assertEquals("Savings Account", response.jsonPath().getString("name"));
        Assert.assertEquals("Save money account", response.jsonPath().getString("description"));
        System.out.println(response.jsonPath().getString("id"));

        //Assert.assertEquals(created, response.jsonPath().getString("created"));
        variables.put("projectId", response.jsonPath().getString("id"));
        variables.put(workspaceId, response.jsonPath().getString("workspaceId"));
    }

//    @Test
//    public void projectsList() {
//        response = RestAssured.given()
//                .header("Authorization", token())
//                .when()
//                .get(workspaceId)
//                .then()
//                .statusCode(200)
//                .log().all()
//                .extract()
//                .response();
//    }
    @Test(dependsOnMethods = {"memberOf", "createProject"})
    public void updateProject() {

        String updateProject = "{\"id\":\""+variables.get("projectId")+"\",\"created\":\"2022-11-09T01:58:24.835Z\",\"lastModified\":\"2022-11-09T01:58:24.835Z\",\"userId\":\""+variables.get("userId")+"\",\"workspaceId\":\""+variables.get("id")+"\",\"name\":\"My Savings Account\",\"description\":\"Save my money account\",\"type\":\"DESIGN\",\"tags\":[]}";

        System.out.println(updateProject);

        response = RestAssured.given()
                .headers("Content-type", "application/json")
                .header("Authorization", token())
                .and()
                .body(updateProject)
                .when()
                .put("/design/projects/" + variables.get("projectId"))
                .then()
                .log().all()
                .extract()
                .response();

        Assert.assertEquals("My Savings Account", response.jsonPath().getString("name"));
        Assert.assertEquals("Save my money account", response.jsonPath().getString("description"));
    }
    @Test(dependsOnMethods = {"memberOf", "createWorkspace", "createProject", "updateProject"})
    public void deleteProject() {

        String deleteProject = "{\"created\":1667959104835,\"description\":\"Save a lot of money account\",\"id\":\""+variables.get("projectId")+"\"," +
                "\"lastModified\":1668558527150,\"name\":\"My Savings Accounts\",\"tags\":[],\"type\":\"DESIGN\",\"userId\":\""+variables.get("userId")+"\"\"," +
                "\"workspaceId\":\""+variables.get("id")+"\"}";

        response = RestAssured.given()
                .headers("Content-type", "application/json")
                .header("Authorization", token())
                .and()
                //.body(deleteProject)
                .when()
                .delete("/design/projects/" + variables.get("projectId"))
                .then()
                .log().all()
                .extract()
                .response();

        System.out.println("deleted " + response);
        Assert.assertEquals(SC_NO_CONTENT, response.statusCode());

    }
    @Test(dependsOnMethods = {"memberOf", "createWorkspace", "createProject", "updateProject"})
    public void deleteWorkspace() {

        String deleteProject = "{\"id\":\"\",\"created\":\"2022-11-09T01:58:24.835Z\",\"lastModified\":\"2022-11-09T01:58:24.835Z\"," +
                "\"userId\":\""+variables.get("userId")+"\",\"workspaceId\":\""+variables.get("id")+"\",\"name\":\"Savings Account\"," +
                "\"description\":\"Save money account\",\"type\":\"DESIGN\",\"tags\":[]}";

        response = RestAssured.given()
                .headers("Content-type", "application/json")
                .header("Authorization", token())
                .and()
                //.body(deleteProject)
                .when()
                .delete("/workspaces/" + variables.get("id"))
                .then()
                .log().all()
                .extract()
                .response();

        System.out.println("deleted " + response);
        Assert.assertEquals(SC_NO_CONTENT, response.statusCode());

    }
}
